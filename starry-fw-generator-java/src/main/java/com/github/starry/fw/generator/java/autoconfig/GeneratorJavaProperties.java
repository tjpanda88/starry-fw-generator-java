package com.github.starry.fw.generator.java.autoconfig;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("starry.generator.java")
public class GeneratorJavaProperties {

}
