package com.github.starry.fw.generator.java.generator.command;

import org.apache.commons.lang3.StringUtils;

import com.github.starry.fw.generator.java.generator.JavaModifierEnum;
import com.github.starry.fw.generator.java.generator.JavaType;
import com.github.starry.fw.generator.java.generator.JavaTypeUtil;

public class JavaBeanClass extends JavaClass {

    public JavaBeanClass(JavaModifierEnum modifier,
            String className, String classComment) {
        super(modifier,className,classComment);
    }

    public JavaBeanClass(
            String className, String classComment) {
        super(className,classComment);
    }

    public void addField(JavaType fieldType,
            String fieldName, String fieldComment) {
        addBlockCommand(createField(fieldType,fieldName,fieldComment));
        addBlockCommand(createSetMethod(fieldType,fieldName,fieldComment));
        addBlockCommand(createGetMethod(fieldType,fieldName,fieldComment));
    }

    public JavaField createField(JavaType fieldType,
            String fieldName,String fieldComment) {
        return new JavaField(fieldType,fieldName,fieldComment);
    }

    public JavaMethod createSetMethod(JavaType fieldType,
            String fieldName,String fieldComment) {

        JavaMethod javaMethod = new JavaMethod(JavaModifierEnum.PUBLIC,new JavaMethodReturn(JavaTypeUtil.createVoid(),""),"set" + StringUtils.capitalize(fieldName),"set"+fieldComment);
        javaMethod.addParam(new JavaMethodParam(fieldType,fieldName,fieldComment));
        javaMethod.addBlockCommand(new NormalCommand("this." + fieldName + " = " + fieldName));
        return javaMethod;
    }

    public JavaMethod createGetMethod(JavaType fieldType,
            String fieldName,String fieldComment) {

        JavaMethod javaMethod = new JavaMethod(JavaModifierEnum.PUBLIC,new JavaMethodReturn(fieldType,fieldComment),"get" + StringUtils.capitalize(fieldName),"get"+fieldComment);

        javaMethod.addBlockCommand(new NormalCommand("return this." + fieldName));
        return javaMethod;
    }


}
