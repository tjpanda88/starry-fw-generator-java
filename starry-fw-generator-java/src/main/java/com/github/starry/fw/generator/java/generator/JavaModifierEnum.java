package com.github.starry.fw.generator.java.generator;


public enum JavaModifierEnum {

    EMPTY(""),PRIVATE("private"),PUBLIC("public"),PROTECTED("protected");

    private String modifier = "";

    private JavaModifierEnum(String modifier) {
        this.modifier = modifier;
    }

    @Override
    public String toString() {
        return modifier;
    }



}
