package com.github.starry.fw.generator.java.generator.command;

import org.apache.commons.lang3.StringUtils;

import com.github.starry.fw.generator.java.api.GeneratorConfig;

public abstract class JavaDocComment extends AbstractSingleCommand {

    private String[] comment = new String[]{};

    public JavaDocComment(String... comment) {
        this.comment = comment;
    }

    protected abstract String getJavaDocHead() ;

    @Override
    protected String getCommandLine(GeneratorConfig config){
        return getJavaDocHead() + " " + StringUtils.join(this.comment, " ");
    }

    @Override
    protected String getLineEnd(){
        return "";
    }
}
