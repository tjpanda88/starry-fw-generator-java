package com.github.starry.fw.generator.java.api.impl;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.util.StringUtils;

import com.github.starry.fw.generator.java.api.GeneratorConfig;
import com.github.starry.fw.generator.java.api.GeneratorJavaService;
import com.github.starry.fw.generator.java.generator.JavaFile;

public class GeneratorJavaServiceImpl implements GeneratorJavaService {

    public void createJava(List<JavaFile> javaFiles, String basePath) {

        for(JavaFile javaFile : javaFiles) {
            String packagePath = javaFile.getPackage();
            String filePath = FilenameUtils.concat(basePath, StringUtils.replace(packagePath, ".", "/"));

            String fileName = javaFile.getClassName() + ".java";

            String fullFileName = FilenameUtils.concat(filePath, fileName);

            try {
                FileUtils.writeLines(new File(fullFileName), javaFile.getSourceLines(new GeneratorConfig()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }



    }
}
