package com.github.starry.fw.generator.java.generator.command;

import com.github.starry.fw.generator.java.api.GeneratorConfig;

public class NormalCommand extends SingleCommand {

    private String commandString;

    public NormalCommand(String commandString) {
        this.commandString = commandString;
    }

    @Override
    protected String getCommandLine(GeneratorConfig config){
        return commandString;
    }



}
