package com.github.starry.fw.generator.java.generator.command;

import com.github.starry.fw.generator.java.api.GeneratorConfig;


public class SingleComment extends AbstractSingleCommand {

    private String comment;

    public SingleComment(String comment) {
        this.comment = comment;
    }


    @Override
    protected String getCommandLine(GeneratorConfig config){
        return comment;
    }

    @Override
    protected String getLineEnd(){
        return "";
    }
}
