package com.github.starry.fw.generator.java.generator;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.github.starry.fw.generator.java.api.GeneratorConfig;
import com.github.starry.fw.generator.java.generator.command.NopCommand;

public class AbstractCommand implements Command {

    private Command command = new NopCommand();

    private List<AbstractCommand> blockCommandList = new ArrayList<AbstractCommand>();

    public AbstractCommand() {

    }

    public AbstractCommand(AbstractCommand command) {
        this.command = command;
    }

    public AbstractCommand(AbstractCommand command,  List<AbstractCommand> blockCommandList) {
        this.command = command;
        this.blockCommandList = blockCommandList;
    }

    public void setCommand(AbstractCommand command) {
        this.command = command;
    }

    public void addBlockCommand(AbstractCommand command) {
        if(command == null) return;
        blockCommandList.add(command);
    }

    protected String getLineStart(){
        return "";
    }

    protected String getLineEnd(){
        return ";";
    }

    protected String getBLockStart(){
        return "{";
    }

    protected String getBLockEnd(){
        return "}";
    }

    protected String getCommandLine(GeneratorConfig config){
        List<String> sourceLines = command.getSourceLines(config);

        if(CollectionUtils.isEmpty(sourceLines)) {
            return "";
        }
        return command.getSourceLines(config).get(0);
    }

    protected String getBLockCommandLineStart(GeneratorConfig config){
        return config.getIndent();
    }

    protected String getBLockCommandLineEnd(GeneratorConfig config){
        return "";
    }

    protected List<String> getBLockCommandLine(AbstractCommand blockCommand, GeneratorConfig config){

        List<String> sourceLines = blockCommand.getSourceLines(config);

        List<String> returnSourceLines = new ArrayList<String>();
        for(String sourceLine : sourceLines) {
            returnSourceLines.add(getBLockCommandLineStart(config) + sourceLine + getBLockCommandLineEnd(config));
        }

        return returnSourceLines;
    }

    public List<String> getSourceLines(GeneratorConfig config) {

        List<String> sourceLines = new ArrayList<String>();

        sourceLines.addAll(getComment(config));

        if(CollectionUtils.isEmpty(blockCommandList)) {
            sourceLines.add(getLineStart() + getCommandLine(config) + getLineEnd());
        } else {
            sourceLines.add(getCommandLine(config) + getBLockStart());

            for(AbstractCommand command : blockCommandList) {
                sourceLines.addAll(getBLockCommandLine(command,config));
            }

            sourceLines.add(getBLockEnd());
        }

        return sourceLines;
    }

    public List<String> getImports() {
        List<String> importLines = new ArrayList<String>();
        importLines.addAll(command.getImports());
        for(AbstractCommand blockCommand : blockCommandList) {
            importLines.addAll(blockCommand.getImports());
        }
        return importLines;
    }

    public List<String> getComment(GeneratorConfig config) {
        return new ArrayList<String>();
    }

}
