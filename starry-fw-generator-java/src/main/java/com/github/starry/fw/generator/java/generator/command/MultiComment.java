package com.github.starry.fw.generator.java.generator.command;

import com.github.starry.fw.generator.java.api.GeneratorConfig;
import com.github.starry.fw.generator.java.generator.AbstractCommand;


public class MultiComment extends AbstractCommand {

    public MultiComment() {
    }

    public MultiComment(String comment) {
        addBlockCommand(new SingleComment(comment));
    }

    public MultiComment(String[] comments) {
        for(String comment : comments) {
            addBlockCommand(new SingleComment(comment));
        }
    }

    @Override
    protected String getBLockCommandLineStart(GeneratorConfig config){
        return " * ";
    }

    @Override
    protected String getBLockStart(){
        return "/**";
    }

    @Override
    protected String getBLockEnd(){
        return " */";
    }
}
