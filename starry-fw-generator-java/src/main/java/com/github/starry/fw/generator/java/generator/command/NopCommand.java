package com.github.starry.fw.generator.java.generator.command;

import java.util.ArrayList;
import java.util.List;

import com.github.starry.fw.generator.java.api.GeneratorConfig;
import com.github.starry.fw.generator.java.generator.Command;


public class NopCommand implements Command {

    public List<String> getSourceLines(GeneratorConfig config) {
        return new ArrayList<String>();
    }

    public List<String> getImports()  {
        return new ArrayList<String>();
    }
}
