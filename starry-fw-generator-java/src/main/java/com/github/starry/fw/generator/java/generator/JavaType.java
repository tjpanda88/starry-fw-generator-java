package com.github.starry.fw.generator.java.generator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class JavaType {

     private String fieldType;

     private List<String> importList = new ArrayList<String>();

     private String defaultValue;

     public JavaType(String fieldType,String defaultValue,String... importString) {
         this.fieldType = fieldType;
         this.defaultValue = defaultValue;
         Collections.addAll(importList, importString);
     }

     public String getDefaultValue() {
         return defaultValue;
     }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public List<String> getImportList() {
        return importList;
    }

    public void setImportList(List<String> importList) {
        this.importList = importList;
    }

    @Override
    public String toString() {
        return fieldType;
    }

    public List<String> getImports() {
        return importList;
    }


}
