package com.github.starry.fw.generator.java.generator;

import java.util.List;

import com.github.starry.fw.generator.java.api.GeneratorConfig;

public interface Command {

    public List<String> getSourceLines(GeneratorConfig config) ;

    public List<String> getImports() ;


}
