package com.github.starry.fw.generator.java.generator.command;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.github.starry.fw.generator.java.api.GeneratorConfig;
import com.github.starry.fw.generator.java.generator.JavaType;


public class JavaMethodReturn extends SingleCommand {

    private JavaType fieldType;
    private String fieldComment;

    public JavaMethodReturn( JavaType fieldType,String fieldComment) {
        super();
        this.fieldType = fieldType;
        this.fieldComment  = fieldComment;
    }

    @Override
    protected String getCommandLine(GeneratorConfig config){
        return fieldType.toString();
    }

    public JavaDocReturn getJavaDoc() {
        if(StringUtils.isEmpty(fieldComment)) {
            return null;
        }
        return new JavaDocReturn(fieldComment);
    }

    @Override
    public List<String> getImports() {
        return fieldType.getImportList();
    }


}
