package com.github.starry.fw.generator.java.autoconfig;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.starry.fw.generator.java.api.GeneratorJavaService;
import com.github.starry.fw.generator.java.api.impl.GeneratorJavaServiceImpl;

@Configuration
public class GeneratorJavaAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(GeneratorJavaService.class)
    public GeneratorJavaService generatorJavaService() {
        GeneratorJavaService generatorJavaService = new GeneratorJavaServiceImpl();
        return generatorJavaService;
    }

}
