package com.github.starry.fw.generator.java.generator.command;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.github.starry.fw.generator.java.api.GeneratorConfig;
import com.github.starry.fw.generator.java.generator.JavaType;


public class JavaMethodParam extends SingleCommand {

    private JavaType fieldType;
    private String fieldName ;
    private String fieldComment;

    public JavaMethodParam( JavaType fieldType,
            String fieldName, String fieldComment) {
        super();
        this.fieldType = fieldType;
        this.fieldName = fieldName;
        this.fieldComment = fieldComment;
    }

    @Override
    protected String getCommandLine(GeneratorConfig config){
        return StringUtils.join(new String[]{fieldType.toString(), fieldName } , " ");
    }

    public JavaDocParam getJavaDoc() {
        return new JavaDocParam(fieldName,fieldComment);
    }

    @Override
    public List<String> getImports() {
        return fieldType.getImportList();
    }


}
