package com.github.starry.fw.generator.java.generator.command;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.github.starry.fw.generator.java.api.GeneratorConfig;
import com.github.starry.fw.generator.java.generator.JavaModifierEnum;
import com.github.starry.fw.generator.java.generator.JavaType;


public class JavaField extends SingleCommand {

    private JavaModifierEnum modifier = JavaModifierEnum.PRIVATE;
    private JavaType fieldType;
    private String fieldName ;
    private String fieldComment;

    public JavaField(JavaModifierEnum modifier, JavaType fieldType,
            String fieldName, String fieldComment) {
        super();
        this.modifier = modifier;
        this.fieldType = fieldType;
        this.fieldName = fieldName;
        this.fieldComment = fieldComment;
    }


    public JavaField(JavaType fieldType,
            String fieldName, String fieldComment) {
        this(JavaModifierEnum.PRIVATE, fieldType, fieldName, fieldComment);
    }

    @Override
    protected String getCommandLine(GeneratorConfig config){
        return StringUtils.join(new String[]{modifier.toString(), fieldType.toString(), fieldName, "=", fieldType.getDefaultValue() } , " ");
    }

    @Override
    public List<String> getComment(GeneratorConfig config) {
        MultiComment multiComment = new MultiComment(fieldComment);
        return multiComment.getSourceLines(config);
    }

}
