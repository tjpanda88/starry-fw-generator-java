package com.github.starry.fw.generator.java.generator.command;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.github.starry.fw.generator.java.api.GeneratorConfig;
import com.github.starry.fw.generator.java.generator.AbstractCommand;
import com.github.starry.fw.generator.java.generator.JavaModifierEnum;

public class JavaClass extends AbstractCommand {

    private JavaModifierEnum modifier = JavaModifierEnum.PUBLIC;

    private String className;

    private String classComment;

    public JavaClass(JavaModifierEnum modifier,
            String className, String classComment) {
        super();
        this.modifier = modifier;
        this.className = className;
        this.classComment = classComment;
    }

    public JavaClass(String className, String classComment) {
        super();
        this.className = className;
        this.classComment = classComment;
    }

    public String getClassName(){
        return className;
    }

    @Override
    protected String getCommandLine(GeneratorConfig config){
        return StringUtils.join(new String[]{modifier.toString(), "class", className} ," ");
    }


    @Override
    public List<String> getComment(GeneratorConfig config) {
        MultiComment multiComment = new MultiComment(classComment);
        return multiComment.getSourceLines(config);
    }

}
