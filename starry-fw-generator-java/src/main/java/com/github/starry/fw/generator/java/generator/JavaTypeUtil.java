package com.github.starry.fw.generator.java.generator;



public class JavaTypeUtil {

     public static JavaType createVoid(){
         return new JavaType("void" , "");
     }

     public static JavaType createString(){
         return new JavaType("String" , "null");
     }

     public static JavaType createInt(){
         return new JavaType("int" , "0");
     }

     public static JavaType createArrayListString(){
         return new JavaType("List<String>" , "new ArrayList<String>()" ,"java.util.List", "java.util.ArrayList");
     }


}
