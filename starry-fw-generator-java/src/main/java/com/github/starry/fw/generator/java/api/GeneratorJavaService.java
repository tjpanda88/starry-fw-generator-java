package com.github.starry.fw.generator.java.api;

import java.util.List;

import com.github.starry.fw.generator.java.generator.JavaFile;

public interface GeneratorJavaService {

    public void createJava(List<JavaFile> javaFiles, String basePath);
}
