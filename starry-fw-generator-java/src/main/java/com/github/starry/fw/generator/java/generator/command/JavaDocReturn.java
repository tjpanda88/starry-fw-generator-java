package com.github.starry.fw.generator.java.generator.command;


public class JavaDocReturn extends JavaDocComment {

    public JavaDocReturn(String... comment) {
        super(comment);
    }

    @Override
    protected String getJavaDocHead(){
        return "@return";
    }
}
