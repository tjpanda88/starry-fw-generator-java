package com.github.starry.fw.generator.java.api;

import org.apache.commons.lang3.StringUtils;

public class GeneratorConfig {

    private int indent = 4;

    public String getIndent() {
        return StringUtils.repeat(" ", indent);
    }
}
