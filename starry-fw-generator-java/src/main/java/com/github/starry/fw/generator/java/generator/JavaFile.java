package com.github.starry.fw.generator.java.generator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.github.starry.fw.generator.java.api.GeneratorConfig;
import com.github.starry.fw.generator.java.generator.command.JavaClass;

public class JavaFile {

    private JavaClass javaClass;
    private String javaPackage;


    public JavaFile(String javaPackage,JavaClass javaClass) {
        super();
        this.javaPackage = javaPackage;
        this.javaClass = javaClass;
    }

    public String getPackage(){
        return javaPackage;
    }

    public String getClassName(){
        return javaClass.getClassName();
    }


    public List<String> getSourceLines(GeneratorConfig config) {

        List<String> sourceLines = new ArrayList<String>();

        sourceLines.add(getPackageCommand());
        sourceLines.add("");
        sourceLines.addAll(getImport());
        sourceLines.add("");
        sourceLines.addAll(javaClass.getSourceLines(config));


        return sourceLines;
    }

    private String getPackageCommand(){
        return "package " + javaPackage;
    }

    public List<String> getImport() {
        List<String> importLines = new ArrayList<String>();

        Set<String> importSets=new HashSet<String>(javaClass.getImports());

        for(String importLine : importSets) {
            importLines.add("import " + importLine);
        }

        return importLines;
    }

}
