package com.github.starry.fw.generator.java.generator.command;


public class JavaDocParam extends JavaDocComment {

    public JavaDocParam(String... comment) {
        super(comment);
    }

    @Override
    protected String getJavaDocHead(){
        return "@param";
    }
}
